Name:           yelp
Epoch:          2
Version:        42.2
Release:        2
Summary:        Help browser for the GNOME desktop
License:        LGPL-2.1-or-later and Apache-2.0 AND GPL-2.0-or-later
URL:            https://wiki.gnome.org/Apps/Yelp
Source:         https://download.gnome.org/sources/%{name}/42/%{name}-%{version}.tar.xz

# https://bugzilla.gnome.org/show_bug.cgi?id=687960
Patch1:         0001-Center-new-windows.patch

BuildRequires:  gcc make bzip2-devel gettext-devel
BuildRequires:  intltool itstool desktop-file-utils
BuildRequires:	pkgconfig(gio-2.0) >= 2.67.4
BuildRequires:	pkgconfig(gio-unix-2.0)
BuildRequires:	pkgconfig(gtk+-3.0) >= 3.13.3
BuildRequires:	pkgconfig(gtk+-unix-print-3.0)
BuildRequires:	pkgconfig(libexslt) >= 0.8.1
BuildRequires:	pkgconfig(libhandy-1) >= 1.5.0
BuildRequires:	pkgconfig(liblzma) >= 4.9
BuildRequires:	pkgconfig(libxml-2.0) >= 2.6.5
BuildRequires:	pkgconfig(libxslt) >= 1.1.4
BuildRequires:	pkgconfig(sqlite3)
BuildRequires:	pkgconfig(webkit2gtk-4.1)
BuildRequires:	pkgconfig(webkit2gtk-web-extension-4.1)
BuildRequires:	pkgconfig(yelp-xsl) >= 41.0

Requires:       yelp-xsl
Obsoletes:      %{name}-libs < %{epoch}:%{version}-%{release}

%description
Yelp is the help viewer in GNOME. It natively views Mallard, DocBook, man, info, and HTML documents.
It can locate documents according to the freedesktop.org help system specification.

Yelp development has led to the development of various tools, and the Mallard and DocBook transformations live in standalone XSLT module.
All of these are under the umbrella name Yelp.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for %{name}.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --disable-static
%make_build

%install
%make_install
%delete_la_and_a

%find_lang %{name}

%check
desktop-file-validate $RPM_BUILD_ROOT%{_datadir}/applications/yelp.desktop

%files -f %{name}.lang
%doc AUTHORS NEWS README.md
%license COPYING
%{_bindir}/*
%{_libdir}/libyelp.so.*
%{_libdir}/yelp/web-extensions/libyelpwebextension.so
%{_datadir}/yelp/
%{_datadir}/metainfo/yelp.appdata.xml
%{_datadir}/applications/yelp.desktop
%{_datadir}/yelp-xsl/xslt/common/domains/yelp.xml
%{_datadir}/glib-2.0/schemas/org.gnome.yelp.gschema.xml
%{_datadir}/icons/hicolor/scalable/apps/org.gnome.Yelp.svg
%{_datadir}/icons/hicolor/symbolic/apps/org.gnome.Yelp-symbolic.svg

%files devel
%{_libdir}/libyelp.so
%{_includedir}/libyelp

%changelog
* Tue Dec 03 2024 Funda Wang <fundawang@yeah.net> - 2:42.2-2
- build with webkitgtk-4.1

* Sat Oct 1 2022 Funda Wang <fundawang@yeah.net) - 2:42.2-1
- Update to 42.2
- Rewrite BuildRequires as pkconfig based on source

* Wed Apr 20 2022 dillon chen <dillon.chen@gmail.com> - 2:42.1-1
- Update to 42.1

* Tue Jun 22 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 2:3.38.3-1
- Upgrade to 2:3.38.3

* Fri Jan 29 2021 jinzhimin <jinzhimin2@huawei.com> 2:3.38.2-1
- Upgrade to 2:3.38.2

* Tue Sep 1 2020 yeah_wang <wangye70@huawei.com> 3.36.0-2
- fix source URL

* Mon Jul 20 2020 chengguipeng<chengguipeng1@huawei.com> 3.36.0-1
- Upgrade to 3.36.0-1

* Mon May 18 2020 wangchen <wangchen137@huawei.com> - 3.34.0-2
- rebuild for yelp

* Fri Oct 11 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.34.0-1
- Package init
